import { Component, OnInit } from "@angular/core";
import * as Konva from "../../../assets/scripts/konva";
import * as Data from "./data";

@Component({
    selector: "app-konva",
    templateUrl: "./konva.component.html",
    styleUrls: ["./konva.component.scss"]
})
export class KonvaComponent implements OnInit {

    private stage: Konva.Stage;
    private group: Konva.Group;
    private layer: Konva.Layer;

    private formattedData = {};

    constructor() { }

    prepareData() {
        for (let i = 0; i < Data.nodes.length; i++) {
            this.formattedData[Data.nodes[i]] = [];
        }
        for (let i = 0; i < Data.edges.length; i++) {
            this.formattedData[Data.edges[i].source].push({ "target": Data.edges[i].target, "weight": Data.edges[i].size });
            this.formattedData[Data.edges[i].target].push({ "target": Data.edges[i].source, "weight": Data.edges[i].size });
        }
        console.log("Formatted Data: ", this.formattedData);
    }

    traverseGraph(currentNode: string, result: any, currentDistance: number) {
        result.totalDistance = currentDistance;
        if (result.visited.length >= Data.nodes.length) { return result; }

        const newPaths = this.formattedData[currentNode].sort((a, b) => a.weight - b.weight);
        let closestNonVisitedNeighbour: any = -1;
        closestNonVisitedNeighbour = newPaths.find(element => !result.visited.includes(element.target));

        if (closestNonVisitedNeighbour === -1) { return -1; }

        result.visited.push(closestNonVisitedNeighbour.target);
        return this.traverseGraph(closestNonVisitedNeighbour.target, result, currentDistance + closestNonVisitedNeighbour.weight);
    }


    resizeStage(stage: Konva.Stage): void {
        const bucket = document.getElementById("bucket");
        if (bucket) {
            const stageWidth = bucket.clientWidth;
            stage.width(stageWidth);
            stage.height(window.innerHeight);
        }
    }

    ngOnInit() {
        const self = this;
        let containerHeight = 1000;
        let containerWidth = 1000;
        try {
            containerWidth = document.getElementById("bucket").clientWidth;
            containerHeight = window.innerHeight;
        } catch (error) {

        }
        this.stage = new Konva.Stage({
            container: "bucket",
            width: containerWidth,
            height: containerHeight,
            draggable: true
        });
        this.group = new Konva.Group({});
        this.layer = new Konva.Layer({});

        this.layer.add(this.group);
        this.stage.add(this.layer);


        window.addEventListener("resize", () => {
            self.resizeStage(self.stage);
        });
        console.log("Data: ", Data);
        this.prepareData();

        const solutions = [];

        for (let i = 0; i < Data.nodes.length; i++) {
            const resultTotal = {
                "visited": [Data.nodes[i]],
                "totalDistance": 0
            };
            const result = this.traverseGraph(Data.nodes[i], resultTotal, 0);
            solutions.push(result);
        }
        console.log("--------------RESULT--------------");
        console.log(solutions);
    }

}
